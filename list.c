#include "list.h"

void push_list(List **ls, char var, double val)
{
    char str[50];
    sprintf(str, "%lf", val);
    if (*ls == NULL)
    {
        *ls = (List*)malloc(sizeof(List));
        if (*ls == NULL)
        {
            printf("memory is not access\n");
            return;
        }
        (*ls)->val = (char*)malloc(strlen(str)+1);
        strcpy((*ls)->val, str);
        (*ls)->var = var;
        (*ls)->next = NULL;
    }
    else
    {
        List *temp = (*ls);
        while (temp->next != NULL && temp->var != var)
            temp = temp->next;
        if (temp->next != NULL)
        {
            free(temp->val);
            temp->val = (char*)malloc(strlen(str)+1);
            strcpy(temp->val, str);
            return;
        }
        else
        {
            temp->next = (List*)malloc(sizeof(List));
            if (temp == NULL)
            {
                printf("memory is not access\n");
                return;
            }
            temp = temp->next;
            temp->var = var;
            temp->val = (char*)malloc(strlen(str)+1);
            strcpy(temp->val, str);
            temp->next = NULL;
        }
    }
}

char *find_and_pop(List **ls, char var)
{
    if (*ls == NULL)
    {
        //printf("List is clear\n");
        return NULL;
    }
    List *temp = *ls;
    while (temp != NULL && temp->var != var)
        temp = temp->next;
    if (temp == NULL)
    {
        printf("Variable was not found\n");
        return NULL;
    }
    char *val = (char*)malloc(strlen(temp->val)+1);
    strcpy(val, temp->val);
    return val;
}

void clear_list(List **ls)
{
    if (*ls == NULL)
        return;
    if ((*ls)->next == NULL)
    {
        free(*ls);
        *ls = NULL;
        return;
    }
    List *temp = *ls;
    while (temp->next->next != NULL)
        temp = temp->next;
    free(temp->next);
    temp->next = NULL;
    clear_list(ls);
}
