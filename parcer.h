#ifndef PARCER_H
#define PARCER_H

#include "queue.h"
#include "stack.h"
#include "list.h"
#include "error.h"

static int pos;
static char *str;
static int isInteg;
static Queue *q;

Queue *parcer(char *sr, List *ls, int isEquat);
int isDigit(char ch);
void returnChar(char ch);
char getChar();
char *getValue();
int _true(char ch, int i);
char *getFunc();
int isCharacter(char ch);
char *isFunc(char* s, int p);

#endif // PARCER_H
