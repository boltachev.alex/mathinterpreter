#include "consol.h"

int consol_start(int argc, char *argv[])
{
    List *ls = NULL;
    setlocale(LC_ALL, "rus");
    if (argc == 1)
    {
        begin(ls);
        return 0;
    }
    int i;
    for (i = 1; i < argc;i++)
    {
        if (argv[i][0] == '-')
        {
            if (argv[i][1] == 'e' && argv[i][2] == '\\')
            {
                int j, a = -1000, b = 1000;
                char *str = (char*)malloc(strlen(argv[i])-2);
                strcpy(str, argv[i++]+3);
                if (i < argc && argv[i][0] == 'a' && argv[i][1] == '=')
                {
                    char *str_a = (char*)malloc(strlen(argv[i])-1);
                    strcpy(str_a, argv[i] + 2);
                    a = atof(str_a);
                    i++;
                }
                if (i < argc && argv[i][0] == 'b' && argv[i][1] == '=')
                {
                    char *str_b = (char*)malloc(strlen(argv[i])-1);
                    strcpy(str_b, argv[i] + 2);
                    b = atof(str_b);
                }
                for (j = 0; j < strlen(str); j++)
                    if (str[j] == '=')
                        break;
                char *t;
                if (str[j + 1] != '0')
                {
                    char *s_t = (char*)malloc(strlen(str) - j + 3);
                    strcpy(s_t + 2, str + j + 1);
                    s_t[0] = '-';
                    s_t[1] = '(';
                    int size = strlen(s_t);
                    s_t[size] = ')';
                    s_t[size+1] = '\0';
                    t = (char*)malloc(j + 2 + strlen(s_t));
                    strncpy(t, str, j);
                    strcpy(t + j, s_t);
                    t[strlen(t)] = '\0';
                }
                else
                {
                    t = (char*)malloc(j + 1);
                    strncpy(t, str, j);
                    t[strlen(t)] = '\0';
                }
                Queue *q = parcer(t, ls, 1);
                if (q == NULL)
                    return 3;
                dichotomy(a, b, q);
            }
            else if (!strcmp(argv[i],"--help"))
            {
                help();
                return 0;
            }
            else if (!strcmp(argv[i],"--file"))
            {
                Queue *q = NULL;
                char var;
                char str[256];
                i++;
                if (i == argc)
                {
                    printf("Неверное количество параметров\n");
                    return 1;
                }
                FILE* pf;
                pf = fopen(argv[i], "r");
                if (pf == NULL)
                {
                    printf("Неудалось открыть файл\n");
                    return 2;
                }
                var = fgetc(pf);
                fgetc(pf);
                fgets(str, 255, pf);
                q = parcer(str, ls, 0);
                if (q == NULL)
                    return 3;
                push_list(&ls, var, calculate(q));
            }
            else if (!strcmp(argv[i], "--inter"))
            {
                begin(ls);
                return 0;
            }
            else if (!strcmp(argv[i], "--print"))
            {
                i++;
                if (i == argc)
                {
                    printf("Неверное количество параметров\n");
                    return 1;
                }
                if (argv[i][1] != '\0')
                {
                    Queue* q = parcer(argv[i], ls, 0);
                    if (q == NULL)
                        return 3;
                    printf ("%f\n", calculate(q));
                }
                else
                {
                    char* rez = find_and_pop(&ls, argv[i][0]);
                    if (rez == NULL)
                        return 4;
                    printf ("%s",rez);
                }
                return 0;
            }
        }
        else
        {
            char sx = argv[i][0];
            char* st = (char*)malloc(strlen(argv[i])-1);
            strcpy(st,argv[i]+2);
            Queue* q = parcer(st,ls, 0);
            if (q == NULL)
                return 3;
            push_list(&ls, sx, calculate(q));
        }
    }
    return 0;
}

void help()
{
    printf("--inter - Запустить интерфейс программы\n");
    printf("--file <Полное имя файла> - Прочитать выражение из файла с заданным именем\n");
    printf("--print <Название переменной или выражение> - Выводит на экран значение заданной переменной или выражения\n");
    printf("<Название переменной>=<значение> - Инициализируется переменная с заданным значением\n");
    printf("<Название переменной>=<выражение> - Переменной приравнивается выражение\n");
    printf("S(a)(b)() - определенный интеграл на интервале от a до b,\nгде a и b задаються числами и выполняеться условие a < b\n");
    printf("Поддерживаемые математические функции:\n");
    printf("  sin() - Синус\n");
    printf("  cos() - Косинус\n");
    printf("  tan() - Тангенс\n");
    printf("  snh() - Гиперболический синус\n");
    printf("  csh() - Гиперболический косинус\n");
    printf("  tnh() - Гиперболический тангенс\n");
    printf("  asn() - Арксинус\n");
    printf("  acs() - Аркккосинус\n");
    printf("  atn() - Арктангенс\n");
    printf("  exp() - Экспоента\n");
    printf("  abs() - Модуль числа\n");
    printf("  fac() - Фактериал\n");
    printf("  log() - Натуральный логарифм\n");
    printf("  log10() - Десятичный логарифм\n");
}
