#include "stack.h"

void push1(STACK1** s, char* str, int l, int pos)
{
    STACK1* next = NULL;
    next = (STACK1*)malloc(sizeof(STACK1));
    if (next != NULL)
    {
        next->pos = pos;
        next->data = (char*)malloc(strlen(str) + 1);
        next->a = l;
        strcpy(next->data, str);
        next->next = *s;
        *s = next;
    }
    else
    {
        printf("memory is not access\n");
    }
}

char* pop1(STACK1** s, int* pos)
{
    if (*s == NULL)
    {
        //printf("STACK1 is clear\n");
        return NULL;
    }
    *pos = (*s)->pos;
    STACK1* b = (*s);
    char* str = (char*)malloc(strlen((*s)->data) + 1);
    strcpy(str, (*s)->data);
    free((*s)->data);
    (*s) = (*s)->next;
    free(b);
    return str;
}

void push2(STACK2** s, double l)
{
    STACK2* next = NULL;
    next = (STACK2*)malloc(sizeof(STACK2));
    if (next != NULL)
    {
        next->a = l;
        next->next = *s;
        *s = next;
    }
    else
    {
        printf("memory is not access\n");
    }
}

double pop2(STACK2** s)
{
    if (*s == NULL)
    {
        //printf("STACK1 is clear\n");
    }
    STACK2* b = (*s);
    double val = (*s)->a;
    (*s) = (*s)->next;
    free(b);
    return val;
}

