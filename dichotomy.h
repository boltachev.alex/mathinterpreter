#ifndef dichotomy_h
#define dichotomy_h

#include "calculate.h"
#include "math.h"

void dichotomy(double a, double b, Queue *input);

#endif
