#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct List
{
    char var;
    char *val;
    struct List *next;
}List;

void push_list(List **ls, char var, double val);
char *find_and_pop(List **ls, char var);
void clear_list(List **ls);

#endif
